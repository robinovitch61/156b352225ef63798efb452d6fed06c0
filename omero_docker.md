1. Clone the example repo and edit this part of [this file](https://github.com/ome/docker-example-omero/blob/master/docker-compose.yml)
```sh
  omeroweb:
    image: "openmicroscopy/omero-web-standalone:5.6" <---- find the name and tag on dockerhub of the one you want
    environment:
      OMEROHOST: omeroserver
    networks:
      - omero
    ports:
      - "4080:4080"
   ```

2. See if that works. Reset docker with:
```sh
cd omero-server-docker
docker-compose down # stops running containers
docker-compose rm # removes stopped containers
docker-compose up # brings up new containers from the images specified
docker-compose up -d # if you don't want logs
```

3. See if image upload works

4. See if the python script upload works

5. See if running a hello world like script works, see if you can figure out where it's running and what python is running


Note:
prune docker images:
```sh
docker system prune -af # something like this
```